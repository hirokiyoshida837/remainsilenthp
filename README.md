# RemainSilentHP


[![pipeline status](https://gitlab.com/HirokiYoshida837/remainsilenthp/badges/master/pipeline.svg)](https://gitlab.com/HirokiYoshida837/remainsilenthp/-/commits/master)

## 開発手順

開発環境立ち上げ
```sh
$ npm run dev
```

手元で静的ページを生成して起動
```sh
$ npm run generate
$ npm run start
```

## TODO

- lint setting
- 