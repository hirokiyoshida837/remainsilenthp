import * as FontAwesome from './build/fontawesome'
import * as GoogleFont from './build/googlefont'

// refs https://ja.nuxtjs.org/faq/github-pages/
const routerBase =
    process.env.DEPLOY_ENV === 'GL_PAGES'
        ? {
            router: {
                base: '/remainsilenthp/'
            }
        }
        : {}

export default {
    // Full Static Generateするので targetをstaticに
    target: 'static',
    components: true,
    generate: {
        dir: "public"
    },
    ...routerBase,

    buildModules: [
        ['@nuxtjs/fontawesome', { component: 'fontAwesome' }],
        ['@nuxtjs/google-fonts'],
    ],
    fontawesome: {
        icons: {
            solid: FontAwesome.solid,
            brands: FontAwesome.brands,
            regular: FontAwesome.regular
        }
    },
    googleFonts: {
        families: GoogleFont.families
    }
}