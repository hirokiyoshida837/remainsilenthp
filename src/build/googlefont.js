const families = {
    Nunito: {
        wght: [200]
    },
    'M+PLUS+Rounded+1c': {
        wght: [300]
    }
}

export { families }