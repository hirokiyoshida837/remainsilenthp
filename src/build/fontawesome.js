const brands = [
    'faBandcamp',
    'faFacebookSquare',
    'faInstagram',
    'faSoundcloud',
    'faSpotify',
    'faTwitter',
]
const regular = ['faStar']
const solid = ['faCog']


export { brands, regular, solid }